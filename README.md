# Proyecto MyMoney

## Antes de empezar

    - Descargar Laragon
    - Verificar que tenga node version >= 18
    - Emulador de Android, o telefono movil
## Opcion de descarga
Descargar este: [Proyecto](https://gitlab.com/tecnoprofe/mymoney/-/archive/main/mymoney-main.zip)
## OPCION 1.  Instalacion via descarga

Ingresa a la carpeta del proyecto
```
cd mymoney-main
```

Descarga las librerias del proyecto
```
npm install
```
Ejecuta la aplicación en un emulador 
```
npx expo start
```
Presionar la letra a. Para iniciar con el emulador de Android. 
![img01](img/ejecucion.png)
Si esta opción no te funciona, presiona w (de web). Pero antes debes de instalar las siguientes librerias:
```
npx expo install react-native-web@~0.18.10 react-dom@18.2.0 @expo/webpack-config@^18.0.1
```

## OPCION 2. Por HTTPS (Es neceesario tener cuenta en gitlab)
Es necesario tener cuenta en gitlab y permiso a la aplicacion. Tambien es necesario recordar su contraseña.
```
git clone https://gitlab.com/tecnoprofe/mymoney.git
cd mymoney
npm install
npx expo start
```