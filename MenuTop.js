import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Home from "./pages/Home";
import Dapc from "./pages/Dapc";
import Gsp from "./pages/Gsp";
import Xjl from "./pages/Xjl";
import { MenuLateral } from './MenuLateral';

const Tab = createMaterialTopTabNavigator();

export const MenuTop = () => {
  //const { top:paddingTop } = useSafeAreaInsets()

  return (
    <Tab.Navigator
      //style={{ paddingTop }}
      sceneContainerStyle={{
        backgroundColor: 'white'
      }}
      tabBarOptions={{
        pressColor: '#bce3c5',
        showIcon: true,
        indicatorStyle: {
          backgroundColor: 'gray'
        },
        style: {
          shadowColor: 'transparent',
          elevation: 0,
          backgroundColor: '#4552F7'
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, focused }) => {

          let iconName = '';
          switch (route.name) {
            case 'MenuLateral':
              iconName = 'calendar'
              break;
            case 'Home':
              iconName = 'calendar'
              break;

            case 'Dapc':
              iconName = 'upload'
              break;

            case 'Gsp':
              iconName = 'laptop'
              break;
            case 'Xjl':
              iconName = 'database'
              break;
          }

          return <Icon name={iconName} size={20} color="white" />
        }
      })}
    >

      <Tab.Screen name="MenuLateral" component={MenuLateral} options={{ title: 'Menu completo' }} />
      <Tab.Screen name="Home" component={Home} options={{ title: 'Home' }} />
      <Tab.Screen name="Dapc" component={Dapc} options={{ title: 'Dapc' }} />
      <Tab.Screen name="Gsp" component={Gsp} options={{ title: 'Gsp' }} />
      <Tab.Screen name="Xjl" component={Xjl} options={{ title: 'Ximena Justiniano' }} />
    </Tab.Navigator>
  );
}