import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome5';


const dataResumen = {
  ingreso: "1000", gasto: "155", completo: "122"
}

const dataGastoTotal = {
  dia: 19, mes: '28-2023', diaNombre: 'lunes', ingreso: 100, gasto: 0
}

const dataDetalleGasto = [
  { categoria: 'Comida', item: 'Hamburguesa', formaPago: 'Tarjeta', monto: '30' },
  { categoria: 'Ocio', item: 'Juegos', formaPago: 'Tarjeta', monto: '15' },
  { categoria: 'Cine', item: 'Soda', formaPago: 'En efectivo', monto: '85' }
]

const dataGastos = [
  { dataGastoTotal, dataDetalleGasto },
  { dataGastoTotal, dataDetalleGasto },
  { dataGastoTotal, dataDetalleGasto },
  { dataGastoTotal, dataDetalleGasto },
  { dataGastoTotal, dataDetalleGasto }
]

const Diario = () => {
  const resumen = dataResumen
  const gastos = dataGastos
  return (
    <ScrollView>
      <Resumen
        ingreso={resumen.ingreso}
        gasto={resumen.gasto}
        completo={resumen.completo} />

      {
        gastos.map((item, index) => {
          return (
            <Gasto
              gastoTotal={item.dataGastoTotal}
              detalleGasto={item.dataDetalleGasto}
              key={index} />
          )
        })
      }

    </ScrollView>
  )
}

const Dia = () => {
  return (
    <GastoForm />
  )
}

const Semana = () => {
  return (
    <View><Text>Semana</Text></View>
  )
}

const Mes = () => {
  return (
    <View><Text>Mes</Text></View>
  )
}

const Total = () => {
  return (
    <View><Text>Total</Text></View>
  )
}

const Tab = createMaterialTopTabNavigator();

export const MenuFecha = () => {
  //const { top:paddingTop } = useSafeAreaInsets()

  return (
    <Tab.Navigator
      //style={{ paddingTop }}
      sceneContainerStyle={{
        backgroundColor: 'white'
      }}
      screenOptions={{
        pressColor: 'green',
        showIcon: true,
        indicatorStyle: {
          backgroundColor: 'green'
        },
        style: {
          shadowColor: 'transparent',
          elevation: 0,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, focused }) => {

          let iconName = '';
          switch (route.name) {
            case 'Diario':
              iconName = 'calendar'
              break;
            case 'Dia':
              iconName = 'calendar'
              break;

            case 'Semana':
              iconName = 'upload'
              break;

            case 'Mes':
              iconName = 'laptop'
              break;
            case 'Total':
              iconName = 'database'
              break;
          }

          return <Icon name={iconName} size={20} color={color} />
        }
      })}
    >

      <Tab.Screen name="Dia" component={Dia} options={{ title: 'Dia' }} />
      <Tab.Screen name="Diario" component={Diario} options={{ title: 'Diario' }} />

      <Tab.Screen name="Semana" component={Semana} options={{ title: 'Semana' }} />
      <Tab.Screen name="Mes" component={Mes} options={{ title: 'Mes' }} />
      <Tab.Screen name="Total" component={Total} options={{ title: 'Total' }} />
    </Tab.Navigator>
  );
}