import { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Button } from 'react-native';
import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { MenuPrincipal } from './MenuPrincipal';

const Drawer = createDrawerNavigator();

export const MenuLateral = () => {
  return (
    <Drawer.Navigator
      initialRouteName="MenuPrincipal"
      drawerContent={(props) => <MenuInterno {...props} />}
    >
      <Drawer.Screen
        name="MenuPrincipal"
        component={MenuPrincipal}
        options={{ title: 'Gastos' }}
      />
    </Drawer.Navigator>
  );
}

const image = "https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png";

const MenuInterno = ({ navigation }) => {
  return (
    <DrawerContentScrollView style={{ paddingBottom: 20 }}>
      {/* Menu */}
      <View style={styles.avatarContainer}>
        <Image
          source={{ uri: image, }}
          style={styles.avatar}
        />
      </View>
      {/* Menu */}
      <Login navigation={navigation} />
    </DrawerContentScrollView>
  )
}

const MenuButton = ({ title, icon, navigation, url }) => {
  return (
    <View style={styles.menuContainer}>
      <TouchableOpacity
        style={styles.menuButton}
        onPress={() => navigation.navigate(url)}
      >
        <View style={styles.menuIconTitle}>
          <Icon name={icon} size={22} color="gray" light style={styles.icon} />
          <Text style={styles.menuTitle} > {title}</Text>
        </View>
        <Icon name="arrow-right" size={22} color="gray" light />
      </TouchableOpacity>
    </View>
  )
}

const Login = (navigation) => {
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');

  const iniciarSession = () => {
    console.log(user, password);
  }

  return (
    <View style={{ marginHorizontal: 30, marginTop: 50 }}>
      <View>
        <Text>User</Text>
        <TextInput style={styles.formInput}
          onChangeText={value => setUser(value)}
          value={user}
        />
      </View>
      <View>
        <Text>Password</Text>
        <TextInput style={styles.formInput}
          secureTextEntry
          onChangeText={value => setPassword(value)}
          value={password}
        />
      </View>
      <View style={{ marginTop: 10 }}>
        <Button title="Login" onPress={iniciarSession} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  avatarContainer: {
    alignItems: 'center',
    marginTop: 25,
    marginBottom: 25
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100
  },
  menuContainer: {
    marginHorizontal: 30,
    marginVertical: 5,
    borderBottomColor: 'gray',
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  menuIconTitle: {
    flexDirection: 'row'
  },
  menuButton: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  menuTitle: {
    fontSize: 16
  },
  icon: {
    width: 35
  },
  formInput: {
    height: 30,
    borderWidth: 1,
    borderColor: 'gray',
    marginBottom: 5,
    paddingHorizontal: 5,
    borderRadius: 8
  },
})
