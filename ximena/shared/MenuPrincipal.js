import { Text, View,Image } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { createStackNavigator } from '@react-navigation/stack';

import { EgresoPage, IngresoPage } from '../page/GastoPage';
import Genera from '../page/Genera'; 
import Ingresos from '../page/Ingresos'; 
import Egresos from '../page/Egresos'; 
import { GastoForm } from '../components/GastoForm';
import  graficas  from '../page/graficas';
import  Reporte  from '../page/Reporte';



const Stack = createStackNavigator();
const BottomTabAndroid = createMaterialBottomTabNavigator();

const GeneraStack = () => {
  return (
    <Stack.Navigator initialRouteName="Genera">
      <Stack.Screen name="Genera" component={Genera}options={{ headerShown: false }} />
      <Stack.Screen name="GastoForm" component={GastoForm}options={{ headerShown: false }} />
      <Stack.Screen name="Reporte" component={Reporte} options={{ title: 'Reporte' }} />
        <Stack.Screen name="graficas" component={graficas} options={{ title: 'Gráficas' }} />
    </Stack.Navigator>
  );
};  


export const MenuPrincipal = () => {
  return (
    <BottomTabAndroid.Navigator
      sceneAnimationEnabled={true}
      barStyle={{
        backgroundColor: 'white'
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, focused }) => {

          let iconName = '';
          switch (route.name) {
            case 'IngresoPage':
              iconName = 'money-bill-wave-alt';
              break;
            case 'EgresoPage':
              iconName = 'cash-register';
              break;
            case 'ReportePage':
              iconName = 'file-invoice-dollar';
              break;
            case 'GeneraStack':
              iconName = 'list';
              break;
          }

          return <Icon name={iconName} size={20} color={color} />
        }
      })}
    >
        <BottomTabAndroid.Screen name="GeneraStack" component={GeneraStack} options={{ title: 'General' }} />
      <BottomTabAndroid.Screen name="IngresoPage" component={Ingresos} options={{ title: 'Ingreso' }} />
      <BottomTabAndroid.Screen name="EgresoPage" component={Egresos} options={{ title: 'Egreso' }} />
    
      <BottomTabAndroid.Screen name="ReportePage" component={Reporte} options={{ title: 'Reportes' }} />
    </BottomTabAndroid.Navigator>
  );
}