import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, Text, StatusBar, ScrollView } from 'react-native';
import axios from 'axios';

const API_URL = 'http://tecnoprofe.com/api/money';

const Elemento = ({ titulo, precio }) => (
  <View style={styles.item}>
    <View style={styles.detalleContainer}>
      <Text style={styles.titulo}>{titulo}</Text>
      <Text style={styles.precio}>{precio}</Text>
    </View>
  </View>
);

const MoneyPage = ({ dataType }) => {
  const [dato, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const respuesta = await axios.get(API_URL);
      setData(respuesta.data);
    } catch (error) {
      console.error(error);
    }
  };

  const renderizarElemento = ({ item }) => {
    if (item.type === dataType) {
      return <Elemento titulo={item.detail} precio={item.price} />;
    }
    return null;
  };

  return (
    <ScrollView style={{ backgroundColor: 'white' }}>
      <View style={styles.contenedor}>
        <FlatList
          data={dato}
          renderItem={renderizarElemento}
          keyExtractor={(item) => item.id.toString()}
          contentContainerStyle={styles.flatlistContainer}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#4287f5',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  detalleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  titulo: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#1a1a1a',
  },
  precio: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#008000',
  },
  fecha: {
    fontSize: 18,
    color: '#1a1a1a',
  },
  flatlistContainer: {
    flexGrow: 1,
  },
});

export const IngresoPage = () => <MoneyPage dataType="I" />
export const EgresoPage = () => <MoneyPage dataType="E" />
