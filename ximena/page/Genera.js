import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, Text, StatusBar, TouchableOpacity, TextInput, Modal } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import axios from 'axios';

const API_URL = 'http://tecnoprofe.com/api/money';

const Elemento = ({ titulo, precio, tipo, id, onDelete, onEdit, creacion }) => {
  const getBackgroundColor = () => {
    if (tipo === 'E') {
        return '#8FE082';
    } else if (tipo === 'I') {
        return '#76BA6C';
    } else {
      return '#4287f5';
    }
  };
  const dateObject = new Date(creacion);
  const shortDate = dateObject.toLocaleDateString();
  return (
    <View style={[styles.item, { backgroundColor: getBackgroundColor() }]}>
      <View style={styles.detalleContainer}>
        <View style={styles.infoContainer}>
          <Text style={styles.headerText}>Detalle</Text>
          <Text style={styles.headerText}>Bs</Text>
          <Text style={styles.headerText}>Fecha</Text>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.titulo}>{titulo}</Text>
          <Text style={styles.precio}>{precio}</Text>
          <Text style={styles.creacion}>{shortDate}</Text>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => onEdit(id)}>
                  <Ionicons name="create-outline" size={18} color="#253B22" />
          <Text style={styles.buttonText}>Editar</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => onDelete(id)}>
                  <Ionicons name="trash-outline" size={18} color="#253B22" />
          <Text style={styles.buttonText}>Eliminar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const Genera = ({ navigation }) => {
  const [datos, setDatos] = useState([]);
  const [sumaTypeI, setSumaTypeI] = useState(0);
  const [sumaTypeE, setSumaTypeE] = useState(0);
  const [resta, setResta] = useState(0);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editingItem, setEditingItem] = useState(null);
  const [newDetail, setNewDetail] = useState('');
  const [newPrice, setNewPrice] = useState('');
  const [newDay, setNewDay] = useState('');
  const [newMonth, setNewMonth] = useState('');
  const [newYear, setNewYear] = useState('');

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      fetchData();
    });

    return unsubscribe;
  }, [navigation]);

  const fetchData = async () => {
    try {
      const response = await axios.get(API_URL);
      const responseData = response.data;

      setDatos(responseData);

      const sumaTypeI = responseData
        .filter(item => item.type === 'I')
        .reduce((acc, item) => acc + parseInt(item.price), 0);

      const sumaTypeE = responseData
        .filter(item => item.type === 'E')
        .reduce((acc, item) => acc + parseInt(item.price), 0);

      setSumaTypeI(sumaTypeI);
      setSumaTypeE(sumaTypeE);

      const resta = sumaTypeI - sumaTypeE;
      setResta(resta);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = async (id) => {
    try {
      await axios.delete(`${API_URL}/${id}`);
      fetchData();
    } catch (error) {
      console.error(error);
    }
  };

  const handleEdit = (id) => {
    const itemToEdit = datos.find(item => item.id === id);
    setEditingItem(itemToEdit);
    setNewDetail(itemToEdit.detail);
    setNewPrice(itemToEdit.price.toString());

    // Obtener componentes de la fecha
    const fecha = new Date(itemToEdit.date);
    setNewDay(fecha.getDate().toString());
    setNewMonth((fecha.getMonth() + 1).toString());
    setNewYear(fecha.getFullYear().toString());

    setIsEditModalOpen(true);
  };

  const handleUpdate = async () => {
    try {
      const updatedElementData = {
        detail: newDetail,
        price: newPrice,
        date: `${newYear}-${newMonth.padStart(2, '0')}-${newDay.padStart(2, '0')}`,
      };

      await axios.put(`${API_URL}/${editingItem.id}`, updatedElementData);
      setIsEditModalOpen(false);
      fetchData();
    } catch (error) {
      console.error(error);
    }
  };

  const renderizarElemento = ({ item }) => (
    <Elemento
      titulo={item.detail}
      precio={item.price}
      tipo={item.type}
      creacion={item.date}
      id={item.id}
      onDelete={handleDelete}
      onEdit={handleEdit}
    />
  );

  return (
    <View style={styles.contenedor}>
      <TouchableOpacity style={styles.addButton} onPress={() => navigation.navigate('GastoForm')}>
        <Ionicons name="add-circle-outline" size={24} color="#ffffff" />
        <Text style={styles.addButtonText}>Añadir Gasto</Text>
      </TouchableOpacity>
      <View style={styles.sumaContainer}>
        <Text style={styles.sumaText}>Ingresos: {sumaTypeI}</Text>
        <Text style={styles.sumaText}>Egresos: {sumaTypeE}</Text>
        <Text style={styles.sumaText}>Saldo Total: {resta}</Text>
      </View>
      <Modal visible={isEditModalOpen} onRequestClose={() => setIsEditModalOpen(false)}>
        <View style={styles.modalContent}>
          <Text style={styles.modalTitle}>Editar Detalle</Text>
          <Text>Detalle</Text>
          <TextInput
            style={styles.input}
            onChangeText={setNewDetail}
            value={newDetail}
            placeholder="Detalle"
          />
          <Text>Precio</Text>
          <TextInput
            style={styles.input}
            onChangeText={setNewPrice}
            value={newPrice}
            keyboardType="numeric"
            placeholder="Precio"
          />
          <Text>Día</Text>
          <TextInput
            style={styles.input}
            onChangeText={setNewDay}
            value={newDay}
            keyboardType="numeric"
            placeholder="Día"
          />
          <Text>Mes</Text>
          <TextInput
            style={styles.input}
            onChangeText={setNewMonth}
            value={newMonth}
            keyboardType="numeric"
            placeholder="Mes"
          />
          <Text>Año</Text>
          <TextInput
            style={styles.input}
            onChangeText={setNewYear}
            value={newYear}
            keyboardType="numeric"
            placeholder="Año"
          />
          <TouchableOpacity style={styles.saveButton} onPress={handleUpdate}>
            <Ionicons name="checkmark-circle-outline" size={24} color="#ffffff" />
            <Text style={styles.buttonText}>Guardar</Text>
          </TouchableOpacity>
        </View>
      </Modal>
      <FlatList
        data={datos}
        renderItem={renderizarElemento}
        keyExtractor={(item) => item.id.toString()}
        contentContainerStyle={styles.flatlistContainer}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 15,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  titulo: {
    fontSize: 24,
    fontWeight: 'bold',
      color: '#253B22',
  },
  precio: {
    fontSize: 20,
    fontWeight: 'bold',
      color: '#253B22',
  },
  tipo: {
    fontSize: 15,
      color: '#253B22',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    margin: 20,
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#1a1a1a',
  },
  flatlistContainer: {
    flexGrow: 1,
  },
  detalleContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  addButton: {
    flexDirection: 'row',
    alignItems: 'center',
      backgroundColor: '#253B22',
    padding: 10,
    margin: 16,
    borderRadius: 10,
  },
  addButtonText: {
    color: '#ffffff',
    fontSize: 18,
    marginLeft: 10,
  },
  sumaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginBottom: 10,
  },
  sumaText: {
    fontSize: 18,
    fontWeight: 'bold',
      color: '#253B22',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
    marginRight: 10,
  },
  deleteButton: {
    backgroundColor: '#F51E00',
  },
  editButton: {
    backgroundColor: '#D49B68',
  },
  buttonText: {
      color: '#253B22',
      fontSize:20,
      marginLeft: 5,
    fontWeight:'bold'
  },
  saveButton: {
    flexDirection: 'row',
    alignItems: 'center',
      backgroundColor: '#F51E00',
    padding: 10,
    margin: 16,
    borderRadius: 10,
  },
});

export default Genera;