import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, Text, StatusBar } from 'react-native';
import axios from 'axios';

const API_URL = 'http://tecnoprofe.com/api/money';

const Elemento = ({ titulo, precio, fecha }) => {
  const dateObject = new Date(fecha);
  const shortDate = dateObject.toLocaleDateString();

  return (
    <View style={styles.item}>
      <View style={styles.detalleContainer}>
        <Text style={styles.titulo}>{titulo}</Text>
        <Text style={styles.precio}>{precio}</Text>
        <Text style={styles.fecha}>{shortDate}</Text>
      </View>
    </View>
  );
};

const Ingresos = ({ navigation }) => {
  const [dato, setData] = useState([]);
  const [sumaTypeI, setSumaTypeI] = useState(0);

  const fetchData = async () => {
    try {
      const respuesta = await axios.get(API_URL);
      setData(respuesta.data);
      const sumaTypeI = respuesta.data
        .filter(item => item.type === 'I')
        .reduce((acc, item) => acc + parseInt(item.price), 0);
      setSumaTypeI(sumaTypeI);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      fetchData();
    });
    return unsubscribe;
  }, [navigation]);

  const renderizarElemento = ({ item }) => {
    if (item.type === 'I') {
      return <Elemento titulo={item.detail} precio={item.price} fecha={item.date} />;
    }
    return null;
  };

  return (
    <View style={styles.contenedor}>
      <View style={styles.sumaContainer}>
        <Text style={styles.sumaText}>Ingresos: {sumaTypeI}</Text>
      </View>

      <View style={styles.detalleContainer}>
        <Text style={styles.titulo}>Detalle</Text>
        <Text style={styles.precio}>Bs</Text>
        <Text style={styles.fecha}>Fecha</Text>
      </View>
      
      <FlatList
        data={dato}
        renderItem={renderizarElemento}
        keyExtractor={(item) => item.id.toString()}
        contentContainerStyle={styles.flatlistContainer}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
      backgroundColor: '#76BA6C',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  detalleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  titulo: {
    fontSize: 24,
    fontWeight: 'bold',
      color: '#253B22',
  },
  precio: {
    fontSize: 20,
    fontWeight: 'bold',
      color: '#253B22',
  },
  fecha: {
      fontSize: 20,
      fontWeight: 'bold',
      color: '#253B22',
  },
  flatlistContainer: {
    flexGrow: 1,
  },
  sumaContainer: {
    alignItems: 'center',
    marginBottom: 5,
  },
  sumaText: {
    fontSize: 18,
    fontWeight: 'bold',
      color: '#253B22',
    marginBottom: 5,
  },
});

export default Ingresos;
