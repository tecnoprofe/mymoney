import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Dimensions, TextInput, Button, FlatList } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
const API_URL = 'http://tecnoprofe.com/api/money';

const Reporte = ({ navigation }) => {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [filterType, setFilterType] = useState('detail');
  const [showIngresos, setShowIngresos] = useState(true);
  const [showEgresos, setShowEgresos] = useState(true);

  useEffect(() => {
    // Realizar solicitud a la API para obtener los datos
    const fetchData = async () => {
      try {
        const response = await axios.get(API_URL);
        setData(response.data);
      } catch (error) {
        console.error('Error al obtener los datos:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    // Aplicar filtro y búsqueda cuando cambien los datos, el término de búsqueda, el tipo de filtro o los filtros de ingresos y egresos
    filterAndSearchData();
  }, [data, searchTerm, filterType, showIngresos, showEgresos]);

  // ...

const filterAndSearchData = () => {
  let filteredResult = data;

  // Filtrar por tipo
  if (filterType === 'detail') {
    filteredResult = filteredResult.filter((item) =>
      item.detail?.toLowerCase().includes(searchTerm.toLowerCase())
    );
  } else if (filterType === 'date') {
    filteredResult = filteredResult.filter((item) =>
      item.date && item.date.includes(searchTerm)
    );
  }

  // Filtrar por ingresos y egresos
  filteredResult = filteredResult.filter((item) => {
    if (item.type === 'I' && showIngresos) {
      return true;
    } else if (item.type === 'E' && showEgresos) {
      return true;
    }
    return false;
  });

  setFilteredData(filteredResult);
};

// ...


  const handleSearchTermChange = (text) => {
    setSearchTerm(text);
  };

  const handleFilterTypeChange = (type) => {
    setFilterType(type);
  };

  const handleShowIngresosChange = () => {
    setShowIngresos(!showIngresos);
  };

  const handleShowEgresosChange = () => {
    setShowEgresos(!showEgresos);
  };

  const renderItem = ({ item }) => {
    return (
      <View style={styles.item}>
        <Text>{item.detail}</Text>
        <Text>{item.price}</Text>
        <Text>{item.type}</Text>
        <Text>{item.date}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Datos filtrados</Text>
      <TextInput
        style={styles.input}
        placeholder="Buscar por nombre o detalle"
        value={searchTerm}
        onChangeText={handleSearchTermChange}
      />
      <View style={styles.filterButtonsContainer}>
        <Button
          title="Filtrar por detalle"
          onPress={() => handleFilterTypeChange('detail')}
                  color={filterType === 'detail' ? '#253B22' : '#D4B166'}
        />
        <Button
          title="Filtrar por fecha"
          onPress={() => handleFilterTypeChange('date')}
                  color={filterType === 'date' ? '#253B22' : '#253B22'}
        />
      </View>
      <View style={styles.filterButtonsContainer}>
        <Button
          title={`Mostrar Ingresos (${showIngresos ? '✓' : ' '})`}
          onPress={handleShowIngresosChange}
                  color={showIngresos ? '#76BA6C' : '#D4B166'}
        />
        <Button
          title={`Mostrar Egresos (${showEgresos ? '✓' : ' '})`}
          onPress={handleShowEgresosChange}
                  color={showEgresos ? '#76BA6C' : '#D4B166'}
        />
      </View>
      <FlatList
        data={filteredData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        style={styles.flatList}
      />
      <Button
        title="Ver Gráficas"
              onPress={() => navigation.navigate('graficas')}
              color="#253B22"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
      fontWeight: 'bold',
      color:'#253B22' ,
    marginBottom: 20,
  },
  input: {
    width: Dimensions.get('window').width - 40,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  filterButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  flatList: {
    marginTop: 20,
    flex: 1,
    width: Dimensions.get('window').width - 40,
    marginBottom: 20,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
});

export default Reporte;