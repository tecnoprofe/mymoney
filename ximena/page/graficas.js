import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { VictoryPie } from 'victory';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
const API_URL = 'http://tecnoprofe.com/api/money';

const Graficas = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    // Realizar solicitud a la API para obtener los datos
    const fetchData = async () => {
      try {
        const response = await axios.get(API_URL);
        setData(response.data);
      } catch (error) {
        console.error('Error al obtener los datos:', error);
      }
    };

    fetchData();
  }, []);

  const calculateTotalByType = (type) => {
    const total = data.reduce((acc, item) => {
      if (item.type === type) {
        acc += parseInt(item.price);
      }
      return acc;
    }, 0);

    return total;
  };

  const generateChartData = () => {
    const totalIngresos = calculateTotalByType('I');
    const totalEgresos = calculateTotalByType('E');

    const chartData = [
      { x: 'Ingreso', y: totalIngresos },
      { x: 'Egreso', y: totalEgresos },
    ];

    return chartData;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Gráfico de pastel (Monto total)</Text>
      <VictoryPie
        data={generateChartData()}
        colorScale={['#B1A5D6', '#D4B166']}
        labels={({ datum }) => `${datum.x}: ${datum.y} Bs`}
        style={{ parent: styles.chartContainer }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  chartContainer: {
    width: Dimensions.get('window').width - 120,
    maxWidth: 380,
  },
});

export default Graficas;