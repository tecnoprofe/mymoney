import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Button } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

const API_URL = 'http://tecnoprofe.com/api/money';

export const GastoForm = ({ dataType }) => {
  const [day, setDay] = useState('');
  const [month, setMonth] = useState('');
  const [year, setYear] = useState('');
  const [price, setPrice] = useState('');
  const [detail, setDetail] = useState('');
  const [transactionType, setTransactionType] = useState('');
  const [transactions, setTransactions] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    retrieveTransactions();
  }, []);

  const retrieveTransactions = async () => {
    try {
      const storedTransactions = await AsyncStorage.getItem('transactions');
      if (storedTransactions !== null) {
        const parsedTransactions = JSON.parse(storedTransactions);
        setTransactions(parsedTransactions);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const saveTransactions = async (newTransactions) => {
    try {
      await AsyncStorage.setItem('transactions', JSON.stringify(newTransactions));
    } catch (error) {
      console.error(error);
    }
  };

  const saveData = async () => {
    try {
      const date = `${year}-${month}-${day}`;
      const data = {
        date: date,
        price: price,
        detail: detail,
        type: transactionType,
      };

      const response = await axios.post(API_URL, data);
      console.log(response.data);

      let newTransactions = [...transactions, response.data];

      setTransactions(newTransactions);
      setDay('');
      setMonth('');
      setYear('');
      setPrice('');
      setDetail('');
      setTransactionType('');

      saveTransactions(newTransactions);

      navigation.navigate('Genera'); // Redireccionar a la lista Genera después de guardar los datos

    } catch (error) {
      console.error(error);
    }
  };

  const deleteTransaction = async (transactionId) => {
    try {
      const response = await axios.delete(`${API_URL}/${transactionId}`);
      console.log(response.data);

      let newTransactions = transactions.filter(
        (transaction) => transaction.id !== transactionId
      );

      setTransactions(newTransactions);
      saveTransactions(newTransactions);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <View>
        <Text style={{ fontSize: 20, textAlign: 'center' }}>
          {dataType !== 'E' ? 'Ingresos' : 'Egresos'}
        </Text>
      </View>
      <View>
        <View style={{ ...styles.shadow, paddingHorizontal: 20, paddingVertical: 15 }}>
          <View style={styles.formDate}>
            <View>
              <Text style={{ color: 'gray' }}>Dia</Text>
              <TextInput
                style={{ ...styles.formInput, width: 50, textAlign: 'center' }}
                onChangeText={value => setDay(value)}
                value={day}
                keyboardType='number-pad'
              />
            </View>
            <View style={{ marginHorizontal: 10 }}>
              <Text style={{ color: 'gray' }}>Mes</Text>
              <TextInput
                style={{ ...styles.formInput, width: 50, textAlign: 'center' }}
                onChangeText={value => setMonth(value)}
                value={month}
                keyboardType='number-pad'
              />
            </View>
            <View>
              <Text style={{ color: 'gray' }}>Año</Text>
              <TextInput
                style={{ ...styles.formInput, width: 70, textAlign: 'center' }}
                onChangeText={value => setYear(value)}
                value={year}
                keyboardType='number-pad'
              />
            </View>
            <View style={{ marginLeft: 10 }}>
              <Text style={styles.formLabel}>Precio</Text>
              <TextInput
                style={{ ...styles.formInput, width: 100, textAlign: 'center' }}
                onChangeText={value => setPrice(value)}
                value={price}
                keyboardType='decimal-pad'
              />
            </View>
          </View>

          <View>
            <Text style={styles.formLabel}>Detalle</Text>
            <TextInput
              style={{ ...styles.formInput, height: 50, padding: 3 }}
              onChangeText={value => setDetail(value)}
              value={detail}
              multiline
            />
          </View>

          <View>
            <Text style={styles.formLabel}>Tipo</Text>
            <TouchableOpacity
              style={[styles.selector, transactionType === 'I' && styles.selectorSelected]}
              onPress={() => setTransactionType('I')}
            >
              <Text style={[styles.selectorText, transactionType === 'I' && styles.selectorTextSelected]}>Ingreso</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.selector, transactionType === 'E' && styles.selectorSelected]}
              onPress={() => setTransactionType('E')}
            >
              <Text style={[styles.selectorText, transactionType === 'E' && styles.selectorTextSelected]}>Egreso</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.formButtonControl}>
            <TouchableOpacity>
              <Button title='Guardar' color='green' onPress={saveData} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  formLabel: {
    width: 100,
    color: 'gray',
  },
  formInput: {
    height: 30,
    borderWidth: 1,
    borderColor: 'gray',
    marginBottom: 5,
    paddingHorizontal: 5,
    borderRadius: 8,
  },
  formButtonControl: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  formDate: {
    flexDirection: 'row',
  },
  selector: {
    width: 120,
    height: 40,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 8,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectorSelected: {
    backgroundColor: 'green',
  },
  selectorText: {
    color: 'gray',
    fontWeight: 'bold',
  },
  selectorTextSelected: {
    color: 'white',
  },
});

export default GastoForm;