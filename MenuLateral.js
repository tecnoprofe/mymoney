import { createDrawerNavigator, DrawerContentScrollView } from '@react-navigation/drawer';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Home from "./pages/Home";
import Acercade from "./pages/Acercade";
import Calculadoraom from "./pages/Calculadoraom";
import Fsp from "./pages/Fsp";
import Jmf from "./pages/Jmf";
import Xjl from "./pages/Xjl";
import Mpg from "./pages/Mpg";
import Omm from "./pages/Omm";
import Jec from "./pages/Jec";
import Dhr from "./pages/Dhr";
import Jjp from "./pages/Jjp";
import Dtm from "./pages/Dtm";
import Gsp from "./pages/Gsp";
import Dmv from "./pages/Dmv";
import Kqs from "./pages/Kqs";
import Dapc from "./pages/Dapc";
import Aop from "./pages/Aop";
import { MenuBotton } from './MenuBotton';
import { MenuTop } from './MenuTop';

const Drawer = createDrawerNavigator();

export const MenuLateral = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Calculadoraom"
      drawerContent={(props) => <MenuInterno {...props} />}
    >
      <Drawer.Screen name="MenuBotton" component={MenuBotton} options={{ title: 'Tabs' }} />
      <Drawer.Screen name="MenuTop" component={MenuTop} options={{ title: 'Tops' }} />

      <Drawer.Screen name="Home" component={Home} options={{ title: 'Home' }} />
      <Drawer.Screen name="Acercade" component={Acercade} options={{ title: 'Acercade' }} />
      <Drawer.Screen name="Calculadoraom" component={Calculadoraom} options={{ title: 'Calculadoraom' }} />
      <Drawer.Screen name="Jmf" component={Jmf} options={{ title: 'Jmf' }} />
      <Drawer.Screen name="Fsp" component={Fsp} options={{ title: 'Fsp' }} />
      <Drawer.Screen name="Xjl" component={Xjl} options={{ title: 'Ximena Justiniano' }} />
      <Drawer.Screen name="Mpg" component={Mpg} options={{ title: 'Mpg' }} />
      <Drawer.Screen name="Omm" component={Omm} options={{ title: 'Omm' }} />
      <Drawer.Screen name="Dapc" component={Dapc} options={{ title: 'Dapc' }} />
      <Drawer.Screen name="Jec" component={Jec} options={{ title: 'Jec' }} />
      <Drawer.Screen name="Dhr" component={Dhr} options={{ title: 'Dhr' }} />
      <Drawer.Screen name="Jjp" component={Jjp} options={{ title: 'Jjp' }} />
      <Drawer.Screen name="Dtm" component={Dtm} options={{ title: 'Dtm' }} />
      <Drawer.Screen name="Gsp" component={Gsp} options={{ title: 'Gsp' }} />
      <Drawer.Screen name="Dmv" component={Dmv} options={{ title: 'Dmv' }} />
      <Drawer.Screen name="Kqs" component={Kqs} options={{ title: 'Kqs' }} />
      <Drawer.Screen name="Aop" component={Aop} options={{ title: 'Aop' }} />
    </Drawer.Navigator>
  );
}

const urlNames = [
  { url: 'MenuBotton', icon: 'laptop', title: 'Tabs' },
  { url: 'MenuTop', icon: 'laptop', title: 'Tops' },
  { url: 'Jmf', icon: 'calendar', title: 'Jmf' },
  { url: 'Fsp', icon: 'upload', title: 'Fsp' },
  { url: 'Xjl', icon: 'laptop', title: 'Ximena Justiniano' },
  { url: 'Mpg', icon: 'database', title: 'Mpg' },
  { url: 'Omm', icon: 'mouse', title: 'Omm' },
  { url: 'Dapc', icon: 'hdd', title: 'Dapc' },
  { url: 'Jec', icon: 'user', title: 'Jec' },
  { url: 'Dhr', icon: 'user-alt', title: 'Dhr' },
  { url: 'Jjp', icon: 'female', title: 'Jjp' },
  { url: 'Dtm', icon: 'user-plus', title: 'Dtm' },
  { url: 'Gsp', icon: 'child', title: 'Gsp' },
  { url: 'Dmv', icon: 'user-circle', title: 'Dmv' },
  { url: 'Kqs', icon: 'walking', title: 'Kqs' },
  { url: 'Aop', icon: 'baby', title: 'Aop' },
].sort();

const image = "https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png";

const MenuInterno = ({ navigation }) => {
  return (
    <DrawerContentScrollView style={{ paddingBottom: 20,backgroundColor: '#4A7CE0'}}>
      {/* Menu */}
      <View style={styles.avatarContainer}>
        <Image
        
          source={{ uri: image, }}
          style={styles.avatar}
        />
      </View>

      <MenuButton title="Home" url="Home" icon="home" navigation={navigation} />
      <MenuButton title="Calculadora" url="Calculadoraom" icon="calculator" navigation={navigation} />

      {/* Opciones de Menu */}
      {
        urlNames.map((url, index) =>
          <MenuButton
            title={url.title}
            url={url.url}
            icon={url.icon}
            navigation={navigation}
            key={index} />
        )
      }

      <MenuButton title="Acerca de.." url="Acercade" icon="eject" navigation={navigation} />
    </DrawerContentScrollView>
  )
}

const MenuButton = ({ title, icon, navigation, url }) => {
  return (
    <View style={styles.menuContainer}>
      
      <TouchableOpacity
        style={styles.menuButton}
        onPress={() => navigation.navigate(url)}
      >
        <View style={styles.menuIconTitle}>
          <Icon name={icon} size={22} color="white" light style={styles.icon} />
          <Text style={styles.menuTitle} > {title}</Text>
        </View>
        <Icon name="arrow-right" size={22} color="white" light />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  avatarContainer: {
    alignItems: 'center',
    marginTop: 25,
    marginBottom: 25
    
    
  },
  
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100
    
  },
  menuContainer: {
    marginHorizontal: 35,
    marginVertical: 5,
    borderBottomColor: '#c6ad71',
    borderBottomWidth: StyleSheet.hairlineWidth
    
  },
  menuIconTitle: {
    flexDirection: 'row'
    
  },
  menuButton: {
    flexDirection: 'row',
    justifyContent: 'space-between'
    
  },
  menuTitle: {
    fontSize: 16
  },
  icon: {
    width: 35,
    
    
  }
})
